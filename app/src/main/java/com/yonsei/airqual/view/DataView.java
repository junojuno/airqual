package com.yonsei.airqual.view;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.yonsei.airqual.R;

import org.androidannotations.annotations.EViewGroup;

@EViewGroup(R.layout.view_data)
public class DataView extends LinearLayout{

    public DataView(Context context) {
        super(context);
    }

    void bind() {

    }

}
