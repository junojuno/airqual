package com.yonsei.airqual.fragment;


import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.yonsei.airqual.R;
import com.yonsei.airqual.adapter.DataListAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.ViewsById;

import java.util.List;

@EFragment(R.layout.fragment_week_data)
public class WeekDataFragment extends Fragment {

    @ViewById(R.id.ivWeekDataCal)
    ImageView ivCalendar;

    @ViewById(R.id.llWeekDataSelectCalcendar)
    LinearLayout llSelectCalendar;

    @ViewById(R.id.lvWeekDatas)
    ListView lvDatas;

    @ViewById(R.id.ivWeekDataGraph)
    ImageView ivGraph;

    @ViewsById({R.id.tvWeekDataCalMon1, R.id.tvWeekDataCalMon2, R.id.tvWeekDataCalMon3 , R.id.tvWeekDataCalMon4, R.id.tvWeekDataCalMon5})
    List<TextView> months;

    @ViewsById({R.id.tvWeekDataCalDate1, R.id.tvWeekDataCalDate2, R.id.tvWeekDataCalDate3 , R.id.tvWeekDataCalDate4, R.id.tvWeekDataCalDate5})
    List<TextView> dates;

    @Bean(DataListAdapter.class)
    DataListAdapter dataListAdapter;

    int currentPage = 1;

    int currentDate = 15;

    @Click(R.id.ivWeekDataCalPrevDate)
    void prevDate() {
        if (currentDate > 1) {
            currentDate--;
            updateCurrentDate();
        }
    }

    @Click(R.id.ivWeekDataCalNextDate)
    void nextDate() {
        if (currentDate < 31) {
            currentDate++;
            updateCurrentDate();
        }
    }

    void updateCurrentDate() {
        int i, weight;
        int date;

        for (i = 0, weight = -2 ; i < 5 ; i++, weight ++) {
            date = currentDate + weight;
            TextView item = dates.get(i);

            if (1 <= date && date <= 31) {
                item.setText(date + "");
                item.setVisibility(View.VISIBLE);
            } else {
                item.setVisibility(View.INVISIBLE);
            }
        }

    }

    @Click(R.id.ivWeekDataGraphPrev)
    void prevGraph() {
        currentPage = 0;
        updateGraph();
    }

    @Click(R.id.ivWeekDataGraphNext)
    void nextGraph() {
        currentPage = 1;
        updateGraph();
    }

    private void updateGraph() {
        int res;

        if (currentPage == 0) {
            res = R.mipmap.graph_before;
        } else {
            res = R.mipmap.graph_now;
        }

        ivGraph.setImageResource(res);
    }

    @Click(R.id.ivWeekDataCal)
    void toggleCalendar() {
        if (llSelectCalendar.getVisibility() == View.GONE) {
            llSelectCalendar.setVisibility(View.VISIBLE);
            ivCalendar.setImageResource(R.mipmap.bt_calendar);
        } else {
            llSelectCalendar.setVisibility(View.GONE);
            ivCalendar.setImageResource(R.mipmap.bt_calendar_done);
        }
    }

    @AfterViews
    void init () {
        lvDatas.setAdapter(dataListAdapter);
        updateCurrentDate();
    }

}
