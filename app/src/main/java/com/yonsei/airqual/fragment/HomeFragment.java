package com.yonsei.airqual.fragment;


import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yonsei.airqual.R;
import com.yonsei.airqual.widget.HoloCircularProgressBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

@EFragment(R.layout.fragment_home)
public class HomeFragment extends Fragment {

    @ViewById(R.id.hcpbHome)
    HoloCircularProgressBar hcpbHome;

    @ViewById(R.id.tvHomeInnerCirclePPM)
    TextView tvInnerPPM;

    @ViewById(R.id.ivHomeLoading)
    ImageView ivLoading;

    @ViewById(R.id.tvHomePPM)
    TextView tvPPM;

    @ViewById(R.id.tvHomePPMLabel)
    TextView tvPPMLabel;

    static boolean isFirst;

    @AfterViews
    void init() {
        isFirst = true;
        loading();
    }

    @UiThread(delay = 15)
    void loading() {
        float rot = ivLoading.getRotation();

        rot += 3;

        if (rot > 360) {
            rot = 0;
        }

        ivLoading.setRotation(rot);
        loading();
    }

    void firstUpdate() {
        hcpbHome.setWheelSize(70);
        hcpbHome.setThumbEnabled(true);
        hcpbHome.setProgressBackgroundColor(Color.TRANSPARENT);
        hcpbHome.setProgressColor(Color.RED);
        isFirst = false;

        tvPPMLabel.setText("ppm");

        hcpbHome.setVisibility(View.VISIBLE);
        tvInnerPPM.setVisibility(View.VISIBLE);
        tvPPMLabel.setVisibility(View.VISIBLE);
        tvPPM.setVisibility(View.VISIBLE);

        ivLoading.setVisibility(View.INVISIBLE);
    }


    public void updateAirqual(float gap) {
        if (isFirst) {
            firstUpdate();
        }

        hcpbHome.setProgress(gap / 200);
        tvInnerPPM.setRotation(hcpbHome.getCurrentRotation() - 13);
        tvInnerPPM.setText(String.format("%dppm", (int) gap));
        tvPPM.setText(((int)gap) + "");
    }

}
