package com.yonsei.airqual.fragment;


import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.widget.NumberPicker;

import com.yonsei.airqual.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_setting)
public class SettingFragment extends Fragment {

    @ViewById(R.id.npSettingPeriod)
    NumberPicker npPeriod;

    @ViewById(R.id.npSettingDate)
    NumberPicker npDate;

    @AfterViews
    void init() {
        npPeriod.setMinValue(0);
        npPeriod.setMaxValue(1);
        npPeriod.setDisplayedValues(new String[] {"주", "달"});
        setDividerColor(npPeriod, R.mipmap.bar_clean_period);

        npDate.setMinValue(1);
        npDate.setMaxValue(31);
        setDividerColor(npDate, R.mipmap.bar_clean_period);
    }

    private void setDividerColor(NumberPicker picker, int res) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    Drawable drawable = getResources().getDrawable(res);
//                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, drawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

}
