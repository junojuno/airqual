package com.yonsei.airqual.fragment;


import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.yonsei.airqual.adapter.GuidePagerAdapter;
import com.yonsei.airqual.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.ViewsById;

import java.util.List;

@EFragment(R.layout.fragment_clean)
public class CleanFragment extends Fragment implements ViewPager.OnPageChangeListener {

    @ViewsById({R.id.tvCleanIndicatorNum1, R.id.tvCleanIndicatorNum2, R.id.tvCleanIndicatorNum3, R.id.tvCleanIndicatorNum4, R.id.tvCleanIndicatorNum5})
    List<TextView> tvIndicatorNums;

    @ViewsById({R.id.vCleanIndicatorLine1, R.id.vCleanIndicatorLine2, R.id.vCleanIndicatorLine3, R.id.vCleanIndicatorLine4})
    List<View> vIndicatorLines;

    @ViewById(R.id.vpClean)
    ViewPager vpClean;

    GuidePagerAdapter adapter;

    @Click(R.id.ivGuidePrev)
    void prev() {
        int cur = vpClean.getCurrentItem();
        if (cur != 0) {
            vpClean.setCurrentItem(cur - 1);
        }
    }

    @Click(R.id.ivGuideNext)
    void next() {
        int cur = vpClean.getCurrentItem();
        if (cur != 4) {
            vpClean.setCurrentItem(cur + 1);
        }
    }

    @AfterViews
    void init() {
        adapter = new GuidePagerAdapter(getActivity(), getChildFragmentManager());

        vpClean.setAdapter(adapter);
        vpClean.addOnPageChangeListener(this);
        vpClean.setCurrentItem(0);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

//        if (position != 0 && position != 5) {
//            vIndicatorLines.get(position - 1).setBackgroundColor(Color.RED);
//            if (prevSelected - 1 >= 0) {
//                vIndicatorLines.get(prevSelected - 1).setBackgroundColor(Color.WHITE);
//            }
//        }

        int i = 0;

        for (;i<=position;i++) {
            tvIndicatorNums.get(i).setBackgroundResource(R.mipmap.bg_guide_number);
            tvIndicatorNums.get(i).setTextColor(getResources().getColor(R.color.white));

            if (i != 0){
                vIndicatorLines.get(i - 1).setBackgroundResource(R.mipmap.clean_number_bar_activated);
            }
        }

        for (;i < 5 ; i++) {
            tvIndicatorNums.get(i).setBackgroundResource(R.mipmap.bg_guide_number_outline);
            tvIndicatorNums.get(i).setTextColor(getResources().getColor(R.color.clean_number));

            vIndicatorLines.get(i - 1).setBackgroundResource(R.mipmap.clean_number_bar_normal);
        }

//    }
//
//        for (i = 0; i < position; i++) {
//            vIndicatorLines.get(i).setBackgroundResource(R.mipmap.clean_number_bar_activated);
//
//        }
//
//        for (; i < 5; i++) {
//            vIndicatorLines.get(i).setBackgroundResource(R.mipmap.clean_number_bar_normal);


    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
