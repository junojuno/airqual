package com.yonsei.airqual.fragment;


import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.yonsei.airqual.R;
import com.yonsei.airqual.adapter.DataListAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_year_data)
public class YearDataFragment extends Fragment {

    @ViewById(R.id.ivYearDataCal)
    ImageView ivCalendar;

    @ViewById(R.id.llYearDataSelectCalcendar)
    LinearLayout llSelectCalendar;

    @ViewById(R.id.lvYearDatas)
    ListView lvDatas;

    @ViewById(R.id.ivYearDataGraph)
    ImageView ivGraph;

    @Bean(DataListAdapter.class)
    DataListAdapter dataListAdapter;

    private int currentPage = 1;

    @Click(R.id.ivYearDataGraphPrev)
    void prevDate() {
        currentPage = 0;
        updateGraph();
    }

    @Click(R.id.ivYearDataGraphNext)
    void nextDate() {
        currentPage = 1;
        updateGraph();
    }

    private void updateGraph() {
        int res;

        if (currentPage == 0) {
            res = R.mipmap.graph_year_before;
        } else {
            res = R.mipmap.graph_year_now;
        }

        ivGraph.setImageResource(res);
    }

    @Click(R.id.ivYearDataCal)
    void toggleCalendar() {
        if (llSelectCalendar.getVisibility() == View.GONE) {
            llSelectCalendar.setVisibility(View.VISIBLE);
            ivCalendar.setImageResource(R.mipmap.bt_calendar);
        } else {
            llSelectCalendar.setVisibility(View.GONE);
            ivCalendar.setImageResource(R.mipmap.bt_calendar_done);

        }
    }


    @AfterViews
    void init () {
       lvDatas.setAdapter(dataListAdapter);
    }

}
