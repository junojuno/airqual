package com.yonsei.airqual.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.yonsei.airqual.activity.MainActivity;
import com.yonsei.airqual.adapter.DataPagerAdapter;
import com.yonsei.airqual.R;
import com.yonsei.airqual.widget.TabFrame;
import com.yonsei.airqual.widget.TextTab;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;


@EFragment(R.layout.fragment_data_tab)
public class DataTabFragment extends Fragment implements ViewPager.OnPageChangeListener {

    @ViewById(R.id.vpDataTab)
    ViewPager vpData;

//    @ViewById(R.id.tfDataTab)
//    TabFrame tfData;

    DataPagerAdapter adapter;

    @AfterViews
    void init() {
        initTabs();
    }

    public void selectPage(int page) {
        vpData.setCurrentItem(page);
    }

    @Background
    void initTabs() {
        adapter = new DataPagerAdapter(getChildFragmentManager());

        vpData.setAdapter(adapter);
        vpData.addOnPageChangeListener(this);
//
//        tfData.addTab(new TextTab(getActivity(), "Week"));
//        tfData.addTab(new TextTab(getActivity(), "Month"));
//        tfData.withViewPager(vpData);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        ((MainActivity)getActivity()).selectSubMenu(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
