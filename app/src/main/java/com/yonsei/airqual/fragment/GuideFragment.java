package com.yonsei.airqual.fragment;

import android.support.v4.app.Fragment;
import android.widget.ImageView;
import android.widget.TextView;

import com.yonsei.airqual.activity.GuideDetailActivity_;
import com.yonsei.airqual.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_guide)
public class GuideFragment extends Fragment {

    @FragmentArg("page")
    int page;

    @FragmentArg("title")
    String title;

    @FragmentArg("imgRes")
    int imgRes;

    @ViewById(R.id.tvGuidePageNum)
    TextView tvPageNum;

    @ViewById(R.id.tvGuideTitle)
    TextView tvTitle;

    @ViewById(R.id.ivGuideImg)
    ImageView ivImg;
//
//    @ViewById(R.id.ivGuidePrev)
//    ImageView ivPrev;
//
//    @ViewById(R.id.ivGuideNext)
//    ImageView ivNext;


    @Click(R.id.ivGuideImg)
    void showDetail() {
        GuideDetailActivity_.intent(getActivity()).page(page).title(title).imgRes(imgRes).start();
    }

    @AfterViews
    void init () {
        tvPageNum.setText(String.valueOf(page));
        tvTitle.setText(title);
        ivImg.setImageResource(imgRes);

    }
}
