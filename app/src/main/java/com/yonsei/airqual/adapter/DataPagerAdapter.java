package com.yonsei.airqual.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.yonsei.airqual.fragment.WeekDataFragment;
import com.yonsei.airqual.fragment.WeekDataFragment_;
import com.yonsei.airqual.fragment.YearDataFragment;
import com.yonsei.airqual.fragment.YearDataFragment_;

import java.util.ArrayList;


public class DataPagerAdapter extends FragmentPagerAdapter {

    ArrayList<Fragment> frags;

    public DataPagerAdapter(FragmentManager fm) {
        super(fm);
        frags =new ArrayList<Fragment>();
        frags.add(WeekDataFragment_.builder().build());
        frags.add(YearDataFragment_.builder().build());
    }

    @Override
    public Fragment getItem(int position) {
        return frags.get(position);
    }

    @Override
    public int getCount() {
        return frags.size();
    }
}
