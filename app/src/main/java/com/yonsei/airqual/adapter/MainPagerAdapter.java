package com.yonsei.airqual.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.yonsei.airqual.fragment.CleanFragment_;
import com.yonsei.airqual.fragment.DataTabFragment_;
import com.yonsei.airqual.fragment.HomeFragment_;
import com.yonsei.airqual.fragment.SettingFragment_;

import java.util.ArrayList;


public class MainPagerAdapter extends FragmentPagerAdapter {

    ArrayList<Fragment> frags;

    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
        frags = new ArrayList<Fragment>();
        frags.add(HomeFragment_.builder().build());
        frags.add(DataTabFragment_.builder().build());
        frags.add(CleanFragment_.builder().build());
        frags.add(SettingFragment_.builder().build());


    }

    @Override
    public Fragment getItem(int position) {
        return frags.get(position);
    }

    @Override
    public int getCount() {
        return frags.size();
    }
}
