package com.yonsei.airqual.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.yonsei.airqual.view.DataView;
import com.yonsei.airqual.view.DataView_;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;

@EBean
public class DataListAdapter extends BaseAdapter {

    @RootContext
    Context context;

    ArrayList<String> datas;

    @AfterInject
    void init () {
        datas = new ArrayList<String>();

        for (int i = 0 ; i < 12 ; i++) {
            datas.add("test1");
        }
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public String getItem(int position) {
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return DataView_.build(context);
    }
}
