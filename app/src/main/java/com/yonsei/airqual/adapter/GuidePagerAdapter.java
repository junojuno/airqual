package com.yonsei.airqual.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.yonsei.airqual.R;
import com.yonsei.airqual.fragment.GuideFragment_;


public class GuidePagerAdapter extends FragmentPagerAdapter {

    private final String[] titles;
    TypedArray imgs;

    public GuidePagerAdapter(Context context, FragmentManager fm) {
        super(fm);

        Resources ress = context.getResources();
        titles = ress.getStringArray(R.array.guide_titles);
        imgs = ress.obtainTypedArray(R.array.guide_imgs);
    }

    @Override
    public Fragment getItem(int position) {
        return GuideFragment_.builder().page(position + 1).title(titles[position]).imgRes(imgs.getResourceId(position, 0)).build();
    }


    @Override
    public int getCount() {
        return 5;
    }
}
