package com.yonsei.airqual.activity;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.yonsei.airqual.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_guide_detail)
public class GuideDetailActivity extends Activity {

    @Extra("page")
    int page;

    @Extra("title")
    String title;

    @Extra("img")
    int imgRes;

    @ViewById(R.id.tvGuideDetailPageNum)
    TextView tvPage;

    @ViewById(R.id.tvGuideDetailTitle)
    TextView tvTitle;

    @ViewById(R.id.ivGuideDetailImg)
    ImageView ivImg;

    @AfterViews
    void init() {
        tvPage.setText(String.valueOf(page));
        tvTitle.setText(title);
        ivImg.setImageResource(imgRes);

    }

//    @ViewById(R.id)
    @Click(R.id.ivGuideDetailX)
    void close() {
        finish();
    }
}
