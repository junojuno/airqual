package com.yonsei.airqual.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.yonsei.airqual.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;


@EActivity(R.layout.activity_splash)
public class SplashActivity extends AppCompatActivity {

    @ViewById(R.id.ivSplashLoading)
    ImageView ivLoading;

    boolean isLoadEnded;

    @AfterViews
    void init() {
        isLoadEnded = false;
        loading();
        goToMain();
    }

    @Background(delay = 3000)
    void goToMain() {
        finish();
        MainActivity_.intent(this).start();
    }

    @UiThread(delay = 20)
    void loading() {

        if (!isLoadEnded) {
            float rotation = ivLoading.getRotation();

            if (rotation >= 360) {
                rotation = 0;
            }

            ivLoading.setRotation(rotation + 4);

            loading();
        }
    }

}
