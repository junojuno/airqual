package com.yonsei.airqual.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.yonsei.airqual.adapter.MainPagerAdapter;
import com.yonsei.airqual.R;
import com.yonsei.airqual.fragment.DataTabFragment;
import com.yonsei.airqual.fragment.HomeFragment;
import com.yonsei.airqual.widget.TabFrame;
import com.yonsei.airqual.widget.TextTab;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.ViewsById;

import java.util.ArrayList;
import java.util.List;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    static final String ACITON_AIRQUAL_UPDATE = "com.yonsei.airqual.ACTION_UPDATE_AIRQUAL";

    @ViewById(R.id.llMainSubMenu)
    LinearLayout llSubMenu;

    @ViewById(R.id.vpMainTab)
    ViewPager vpMain;

    @ViewsById({R.id.ivSubMenuWeekMenuSelector, R.id.ivSubMenuYearMenuSelector})
    List<ImageView> selectors;

    @ViewsById({R.id.ivSubMenuWeekMenu, R.id.ivSubMenuYearMenu})
    List<ImageView> submenus;

    @ViewsById({R.id.ibtnTabHome, R.id.ibtnTabCalendar, R.id.ibtnTabClean, R.id.ibtnTabSetting})
    List<ImageButton> ibtnTabs;

    @ViewsById({R.id.ivTabSelectedBgHome, R.id.ivTabSelectedBgCalendar, R.id.ivTabSelectedBgClean, R.id.ivTabSelectedBgSetting})
    List<ImageView> ivTabBgs;

    int tabResIdNormal[] = {R.mipmap.icon_home, R.mipmap.icon_calendar, R.mipmap.icon_clean, R.mipmap.icon_setting};
    int tabResIdSelected[] = {R.mipmap.icon_home_clicked, R.mipmap.icon_calendar_clicked, R.mipmap.icon_clean_clicked, R.mipmap.icon_setting_clicked};

    MainPagerAdapter adapter;

    int selected;

    int cntUpdated = 0;

    private UpdateAirqualReceiver updateReceiver;
    private boolean isStopedService;

//    ArrayList<Double> insideQuals;
//    ArrayList<Double> outsideQuals;

    @AfterViews
    void init() {
        vpMain.setOffscreenPageLimit(5);
        initTabs();
        initReceiver();
    }

    private void initReceiver() {
        updateReceiver = new UpdateAirqualReceiver();
    }

    @Background
    void initTabs() {
        adapter = new MainPagerAdapter(getSupportFragmentManager());

        vpMain.setAdapter(adapter);
        vpMain.addOnPageChangeListener(this);

        vpMain.setCurrentItem(0);
        updateTab(0);

//        insideQuals = new ArrayList<Double>();
//        outsideQuals = new ArrayList<Double>();
//        vpMain.setOnTouchListener(null);
//        vpMain.beginFakeDrag();

//        tfMain.addTab(new TextTab(this, "Home"));
//        tfMain.addTab(new TextTab(this, "Data"));
//        tfMain.addTab(new TextTab(this, "Clean"));
//        tfMain.addTab(new TextTab(this, "Setting"));
//        tfMain.withViewPager(vpMain);
    }

    @Click({R.id.rlMainSubMenuWeek, R.id.rlMainSubMenuYear})
    void subMenuClick(View view) {
        int pos = 0;

        if (view.getId() == R.id.rlMainSubMenuWeek) {
            pos = 0;
        } else {
            pos = 1;
        }

        selectSubMenu(pos);
//        if (view.getId() == R.id.rlMainSubMenuWeek) {
//            selected = 0;
//
//            submenus.get(0).setImageResource(R.mipmap.text_week_clicked);
//            submenus.get(1).setImageResource(R.mipmap.text_year);
//
//            selectors.get(0).setVisibility(View.VISIBLE);
//            selectors.get(1).setVisibility(View.INVISIBLE);
//        } else {
//            selected = 1;
//
//            submenus.get(0).setImageResource(R.mipmap.text_week);
//            submenus.get(1).setImageResource(R.mipmap.text_year_clicked);
//
//            selectors.get(0).setVisibility(View.INVISIBLE);
//            selectors.get(1).setVisibility(View.VISIBLE);
//        }
//
//        ((DataTabFragment)(adapter.getItem(1))).selectPage(selected);
    }

    public void selectSubMenu(int position) {
        if (position == 0) {
            submenus.get(0).setImageResource(R.mipmap.text_week_clicked);
            submenus.get(1).setImageResource(R.mipmap.text_year);

            selectors.get(0).setVisibility(View.VISIBLE);
            selectors.get(1).setVisibility(View.INVISIBLE);
        } else {
            submenus.get(0).setImageResource(R.mipmap.text_week);
            submenus.get(1).setImageResource(R.mipmap.text_year_clicked);

            selectors.get(0).setVisibility(View.INVISIBLE);
            selectors.get(1).setVisibility(View.VISIBLE);
        }

        ((DataTabFragment)(adapter.getItem(1))).selectPage(position);
    }

    @Click({R.id.ibtnTabHome, R.id.ibtnTabCalendar, R.id.ibtnTabClean, R.id.ibtnTabSetting})
    void tabClick(View view) {
        int position = 0;

        switch (view.getId()) {
            case R.id.ibtnTabHome:
                position = 0;
                break;
            case R.id.ibtnTabCalendar:
                position = 1;
                break;
            case R.id.ibtnTabClean:
                position = 2;
                break;
            case R.id.ibtnTabSetting:
                position = 3;
                break;
        }

        vpMain.setCurrentItem(position);
        updateTab(position);
    }

    private void updateTab(int position) {
        if (position == 1) {
            llSubMenu.setVisibility(View.VISIBLE);
            vpMain.setBackgroundResource(R.mipmap.bg_inner_circle_2);
        } else {
            llSubMenu.setVisibility(View.INVISIBLE);
            vpMain.setBackgroundColor(getResources().getColor(R.color.body));
        }

        ibtnTabs.get(selected).setImageResource(tabResIdNormal[selected]);
        ivTabBgs.get(selected).setVisibility(View.GONE);

        ibtnTabs.get(position).setImageResource(tabResIdSelected[position]);
        ivTabBgs.get(position).setVisibility(View.VISIBLE);

        selected = position;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        updateTab(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    void startAirqualService() {
        registerReceiver(updateReceiver, new IntentFilter(ACITON_AIRQUAL_UPDATE));
        startService(new Intent("com.yonsei.airqual.bluetooth.BluetoothService"));

        isStopedService = false;
    }

    void stopAirqualService() {
        if (!isStopedService) {
            unregisterReceiver(updateReceiver);
            stopService(new Intent("com.yonsei.airqual.bluetooth.BluetoothService"));
            isStopedService = true;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        startAirqualService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopAirqualService();
    }


    public class UpdateAirqualReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            double airconQuality = Double.MIN_VALUE;
            double outsideQuality = Double.MIN_VALUE;

            if (intent.hasExtra("AIRCON_QUALITY")) {
                airconQuality = intent.getDoubleExtra("AIRCON_QUALITY", -1);
            }

            if (intent.hasExtra("OUTSIDE_QUALITY")) {
                outsideQuality = intent.getDoubleExtra("OUTSIDE_QUALITY", -1);
            }

            float gap = (float) Math.abs(airconQuality - outsideQuality);
            gap = gap > 200 ? 200 : gap;

            updateAirqual(gap);

            if (cntUpdated ++ == 0) {
                String ticker = "에어퀄 알림이 도착했습니다.";
                String title = "에어퀄";
                String content = getNotiMsg(gap);
                sendNotification(ticker, title, content);
            }

            Log.e("Airquality Update~!", airconQuality + " / " + outsideQuality);
        }

        @NonNull
        private String getNotiMsg(float gap) {
            String content;

            if (gap >= 171) {
                content = "위험: 위험! 에어컨 청소를 적극 권장합니다.";
            } else if (gap >= 51) {
                content = "주의: 주의! 에어컨 청소 시기가 다가오고 있습니다.";
            } else {
                content = "쾌적: 안심하시고 시원한 에어컨 바람을 즐기세요~";
            }

            return content;
        }
    }

    @UiThread
    void sendNotification(String ticker, String title, String msg) {
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.aqual_icon)
                .setTicker(ticker)
                .setWhen(System.currentTimeMillis())
                .setNumber(10)
                .setContentTitle(title)
                .setContentText(msg)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .setAutoCancel(true);

        nm.notify(111, builder.build());
    }

    public void updateAirqual(float gap) {
        ((HomeFragment)adapter.getItem(0)).updateAirqual(gap);

    }

}
