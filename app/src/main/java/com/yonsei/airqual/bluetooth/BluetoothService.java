package com.yonsei.airqual.bluetooth;

import android.app.Activity;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.UUID;

/**
 * Created by junojuno on 2015. 11. 19..
 */
public class BluetoothService extends Service {

    byte delimiter = 10;
    boolean stopWorker = false;
    int readBufferPosition = 0;
    byte[] readBuffer = new byte[1024];

    InputStream inStream;
    Thread workerThread;

    static final String ACITON_GET_DATA = "com.yonsei.airqual.ACTION_GET_DATA";
    static final String ACITON_AIRQUAL_UPDATE = "com.yonsei.airqual.ACTION_UPDATE_AIRQUAL";

    //    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
    private static final String TARGET_DEVICE = "HC-06";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        Log.e("onStart", "YES");
        initBle();
        initReceiver();
    }

    private void initReceiver() {
        workerThread = new Thread(new Runnable() {

            public void run() {
                boolean isEnded = false;

                while (!Thread.currentThread().isInterrupted() && !stopWorker && !isEnded) {
                    try {
                        int bytesAvailable = inStream.available();

                        if (bytesAvailable > 0) {
                            byte[] packetBytes = new byte[bytesAvailable];
                            inStream.read(packetBytes);

                            for (int i = 0; (i < bytesAvailable) && !isEnded; i++) {
                                byte b = packetBytes[i];
                                if (b == delimiter) {
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");

                                    String[] slices = data.split(",");
                                    readBufferPosition = 0;

                                    if (!isDamagedPacket(slices)) {
                                        sendBroadcast(ACITON_AIRQUAL_UPDATE, slices);
                                        isEnded = false;
                                    }

                                } else {
                                    readBuffer[readBufferPosition++] = b;
                                }

                            }
                        }

                    } catch (IOException ex) {
                        stopWorker = true;
                    }
                }


            }
        });
    }

    private boolean isDamagedPacket(String[] slices) {
        if (slices == null) {
            return true;
        } else if (slices.length < 2) {
            return true;
        } else if (slices[0] == null || slices[0].length() == 0 || slices[0].isEmpty()) {
            return true;
        } else if (slices[1] == null || slices[1].length() == 0 || slices[1].isEmpty()) {
            return true;
        }

        return false;
    }

    private void sendBroadcast(String action, String[] slices) {
        Intent intent = new Intent(action);

        intent.putExtra("AIRCON_QUALITY", Double.valueOf(slices[0]));

        if (slices.length == 2) {
            intent.putExtra("OUTSIDE_QUALITY", Double.valueOf(slices[1]));
        }

        sendBroadcast(intent);
    }

    private void initBle() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter); // Don't forget to unregister during onDestroy
        bluetoothAdapter.startDiscovery();
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                Log.e("Bluetooth", "Device Found");
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String address = device.getAddress();
                Log.e("Bluetooth", "Name : " + device.getName());
                Log.e("Bluetooth", "Address : " + address);

                if (!device.getName().isEmpty() && device.getName().equals(TARGET_DEVICE)) {
                    try {
                        BluetoothSocket tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
                        tmp.connect();
                        inStream = tmp.getInputStream();
                        Log.e("Bluetooth", "Connected with " + device.getName());

                        workerThread.start();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);

        try {
            Log.e("BluetoothService", "Destory");
            if (workerThread != null && workerThread.isAlive()) {

                workerThread.interrupt();
                Log.e("BluetoothService", "Worker Destory");
            }

            if (inStream != null) {
                inStream.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
