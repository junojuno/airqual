package com.yonsei.airqual.widget;

import android.view.View;

/**
 *  Tab Interface.
 */
public interface Tab {
    View getView();
}
