package com.yonsei.airqual.widget;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * Tab Frame. include many tabs
 */
public class TabFrame extends LinearLayout {

    private ViewPager vp;

    private int selectedPos;

    public TabFrame(Context context) {
        super(context);
    }

    public TabFrame(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TabFrame(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void addTab(Tab tab) {
        View view = tab.getView();

        LayoutParams params = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.weight = 1;

        view.setLayoutParams(params);
        view.setTag(getChildCount());

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer position = (Integer) view.getTag();

                if (vp != null) {
                    vp.setCurrentItem(position);
                }
            }
        });


        this.addView(view);
    }

    /**
     * make relation with Viewpager. pageChangeListener
     * @param vp
     */
    public void withViewPager(ViewPager vp) {
        this.vp = vp;

        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                updateSelectedTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        updateSelectedTab(0);
    }

    private void updateSelectedTab(int position) {
        getChildAt(selectedPos).setBackgroundColor(Color.WHITE);
        getChildAt(position).setBackgroundColor(Color.BLUE);
        selectedPos = position;
    }

    public int getSelectedPos() {
        return selectedPos;
    }

    public void setSelectedPos(int selectedPos) {
        this.selectedPos = selectedPos;
    }

}
