package com.yonsei.airqual.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

/**
 * Created by junojuno on 2015. 10. 26..
 */
public class TextTab extends TextView implements Tab {

    public TextTab(Context context, String text) {
        super(context);
        setText(text);
    }

    public TextTab(Context context) {
        super(context);
    }

    public TextTab(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextTab(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public View getView() {
        setGravity(Gravity.CENTER);
        return this;
    }
}
