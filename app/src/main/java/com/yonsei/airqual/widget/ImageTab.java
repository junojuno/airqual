package com.yonsei.airqual.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by junojuno on 2015. 10. 26..
 */
public class ImageTab extends ImageView implements Tab {

    public ImageTab(Context context, int resId) {
        super(context);
        setImageResource(resId);
    }

    public ImageTab(Context context) {
        super(context);
    }

    public ImageTab(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageTab(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public View getView() {
//        setGravity(Gravity.CENTER);
        return this;
    }
}
